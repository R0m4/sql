1. Buat Database
create database penjualan_film;
2. Membuat Database
create table genre(
    -> id int(8) primary key auto_increment,
    -> nama varchar(30)
    -> );
3. Memasukkan Data pada Table
create table film(
    -> id int(8) primary key auto_increment,
    -> judul varchar(60),
    -> harga int(20),
    -> genre_id int(8),
    -> foreign key(genre_id) references genre(id)
    -> );
4. Mengambil Data dari Database
a. insert into genre(nama) values("action"),("Drama"),("Komedi");
b. insert into film(judul,harga,genre_id) values("The raid", 30000, 1), ("Warkop DKI", 40000, 3), ("Headshot", 35000, 1);
select id,judul, harga from film;
select * from film where harga>30000;
c. select * from film where judul like '%rai%';
5. Mengubah Data dari Database
select film.id, film.judul, film.harga, film.genre_id, genre.nama as kategori from film inner join genre on film.genre_id = genre.id;
update film set harga=50000 where id=2;